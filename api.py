#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
from ast import arguments
from curses.ascii import isalpha
from modulefinder import STORE_GLOBAL
import re
import time
import json
import datetime
import logging
import hashlib
from typing_extensions import Required
import uuid
from attr import has
import redis

from argparse import ArgumentParser
from http.server import BaseHTTPRequestHandler, HTTPServer
from http import HTTPStatus
from weakref import WeakKeyDictionary
from xmlrpc.client import Boolean
from numpy import isin
from pyrsistent import v

import scoring

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}

def field_obj_initializer(self, required, nullable):
    self.data = WeakKeyDictionary()
    self.required = required
    self.nullable = nullable

class Field:
    def __init__(self, required):
        self.data = WeakKeyDictionary()
        self.required = required

    def __get__(self, inst, owner):
        return self.data.get(inst, None)

    @abc.abstractmethod
    def validate(self, val):
        pass

    def __set__(self, inst, val):
        if self.validate(val):
            self.data[inst] = val
        else:
            s = "Incorrect field value: %s" % self.__class__.__name__
            logging.exception(s)
            raise ValueError(s)
    
        
class CharField(Field):
    def __init__(self, required, nullable):
        super().__init__(required)
        self.nullable = nullable

    def validate(self, val) -> bool:
        return not self.required if val == None else isinstance(val, str)
            

class ArgumentsField(CharField):
    def validate(self, val) -> bool:
        return not self.required if val == None else isinstance(val, dict)

class EmailField(CharField):
    def validate(self, val):
        return not self.required if val == None else re.match(r'^\w+@\w+\.\w+$', val) != None


class PhoneField(CharField):
    def validate(self, val):
        if val == None:
            return not self.required
        else:
            val_str = str(val)
            return val_str.isdigit() and (7 <= len(val_str) <= 11) and (val_str[0] == '7')

class DateField(CharField):
    DATE_FORMAT = "%d.%m.%Y"

    @classmethod
    def is_date_valid(cls, val):
        if val != None:
            try:
                _ = time.strptime(val, DateField.DATE_FORMAT)
            except ValueError:
                logging.exception("Date is not valid: %s" % str(val))
                is_valid = False
            else:
                is_valid = True
        return is_valid

    def validate(self, val):
        return not self.required if val == None else self.is_date_valid(val)

class BirthDayField(DateField):
    def validate(self, val):
        return not self.required if val == None else DateField.is_date_valid(val) and (time.gmtime().tm_year - time.strptime(val, DateField.DATE_FORMAT).tm_year <= 70)

class GenderField(CharField):
    def validate(self, val):
        return not self.required if val == None else isinstance(val, int) and (val in GENDERS.keys())


class ClientIDsField(Field):
    def validate(self, val):
        return not self.required if val == None else (isinstance(val, list) and len(val) and all([isinstance(elem, int) for elem in val]))

class ReqMeta(type):
    def __new__(cls, name, bases, attrs):
        attrs['field_names'] = [k for k, v in attrs.items() if hasattr(v, 'validate')]
        return type.__new__(cls, name, bases, attrs)

class Request(metaclass=ReqMeta):
    def __init__(self, args):
        args_local = dict(args)
        for field_name in self.field_names:
            setattr(self, field_name, args_local.get(field_name))
            if field_name in args.keys():
                del args_local[field_name]
        if len(args_local) > 0:
            logging.warning("Unknown fields: ", args_local)

class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(required=True)
    date = DateField(required=False, nullable=True)

    def __init__(self, args, is_admin, store):
        super(ClientsInterestsRequest, self).__init__(args)
        self.client_interests = { str(id): scoring.get_interests(store, id) for id in self.client_ids }

    def __call__(self, ctx):
        if ctx != None:
            ctx["has"] = [field for field in self.field_names if getattr(self, field) != None]
            ctx["nclients"] = len(self.client_ids)
        return self.client_interests, HTTPStatus.OK

class ScoreDesc:
    def __init__(self):
        self.data = WeakKeyDictionary()

    def __set__(self, inst, val):
        if (inst.first_name and inst.last_name) \
            or (inst.phone and inst.email) \
            or ((inst.gender != None) and inst.birthday):
            self.data[inst] = val
        else:
            s = "Incorrect %s" % inst.__class__.__name__
            logging.exception(s)
            raise ValueError(s)

    def __get__(self, inst, owner=None):
        return self.data[inst]

class OnlineScoreRequest(Request):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)
    score = ScoreDesc()

    def __init__(self, args : dict, is_admin, store):
        super(OnlineScoreRequest, self).__init__(args)
        self.score = 42 if is_admin else scoring.get_score(store, self.phone, self.email, self.birthday, self.gender, self.first_name, self.last_name)

    def __call__(self, ctx):
        if ctx != None:
            ctx["has"] = [field for field in self.field_names if getattr(self, field) != None]
        return { "score" : self.score }, HTTPStatus.OK


class MethodRequest(Request):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN

def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode('utf-8')).hexdigest()
    else:
        digest = hashlib.sha512((request.account + request.login + SALT).encode('utf-8')).hexdigest()
    if digest == request.token:
        return True
    return False

def get_request_handler_class(request_body):
    REQUEST_HANDLER_CLASS_MAP = { 'online_score' : OnlineScoreRequest, 'clients_interests' : ClientsInterestsRequest }
    method_name = request_body.get('method')
    if method_name:
        return REQUEST_HANDLER_CLASS_MAP.get(method_name)
    
def method_handler(request, ctx, store):
    response = {}
    try:
        request_body = request['body']
        method_req = MethodRequest(request_body)
    except Exception as e:
        code = HTTPStatus.BAD_REQUEST
        logging.exception("Could not compose request from request body. Error: %s", e)
        response = { "error" : "Request body is not correct: %s" % e }
    else:
        if check_auth(method_req):
            req_handler_class = get_request_handler_class(request_body) if request_body else None
            if req_handler_class:
                try:
                    request_obj = req_handler_class(request_body.get('arguments'), method_req.is_admin, store)
                except ValueError:
                    err = "Invalid 'arguments' field"
                    logging.exception("Invalid arguments field")
                    response = { "error" : err }
                    code = HTTPStatus.BAD_REQUEST
                else:
                    response, code = request_obj(ctx)
            else:
                err = "Invalid 'method' field"
                logging.exception(err)
                response = { "error" : err }
                code = HTTPStatus.BAD_REQUEST
        else:
            logging.warning("Authorization failed")
            response = { "error" : "Autorization failed" }
            code = HTTPStatus.FORBIDDEN
    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, HTTPStatus.OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = HTTPStatus.BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = HTTPStatus.INTERNAL_SERVER_ERROR
            else:
                code = HTTPStatus.NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code == HTTPStatus.OK:
            r = { "response": response, "code": code }
        else:
            r = { "error": response.get("error") or HTTPStatus(code).description, "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode('utf8'))


if __name__ == "__main__":
    argparser = ArgumentParser()
    argparser.add_argument("-p", "--port", action="store", type=int, default=8080)
    argparser.add_argument("-l", "--log", action="store", default=None)
    args = argparser.parse_args()
    logging.basicConfig(filename=args.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", args.port), MainHTTPHandler)
    logging.info("Starting server at %s" % args.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
